// return random value between min dan max
function getRandomValue(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

let vm = Vue.createApp({
    el: '#game',
    data() {
        return {
            isWinner: false,
            playerHealth: 100,
            monsterHealth: 100,
            msg: [],
        }
    },
    watch: {
        // listening
        playerHealth(value) {
            if (value <= 0 && ! this.isWinner) {
                this.isWinner = true;
                console.log('Player lost');
            }
        },
        monsterHealth(value) {
            if (value <= 0 && ! this.isWinner) {
                this.isWinner = true;
                console.log('Monster lost');
            }
        }
    },
    methods: {
        attack: function() {
            console.log('attack');
            // player attack monster
            let damage1 = getRandomValue(5, 10);
            this.monsterHealth -= damage1;
            this.log('player', damage1, this.monsterHealth);

            // monster attack player
            let damage2 = getRandomValue(5, 10);
            this.playerHealth -= damage2;
            this.log('monster', damage2, this.playerHealth);
        },
        log(who, val, health) {
            this.msg.push(`${who} attack with ${val} stroke Opponent health = ${health}`);
        },
        specialAttack: function() {

        }
    }
});

vm.mount('#game');