<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/sample1', function() {
    $person = new stdClass();
    $person->name = 'Abu';
    $person->email = 'abu@gmail.com';

    $person2 = new stdClass();
    $person2->name = 'John Doe';
    $person2->email = 'john@gmail.com';

    $arr = [$person, $person2];
    return $arr;
});