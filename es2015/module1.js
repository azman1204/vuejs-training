// jika ada "export", maka file js ini dianggap module
// apa yg boleh export: function, class, var

export function bet(race, pony) {
    return `Bet : ${race} ${pony}`;
}

export function start(race) {
    return `Start : ${race}`;
}

// default hanya boleh ada satu shj dlm 1 module
export default class Pony {
    cetak() {
        return "This is a pony";
    }
}

export const nama = 'azman';