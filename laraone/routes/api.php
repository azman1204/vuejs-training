<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\FilmController;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// user web service
Route::get('/users', [UserController::class, 'listing']);

// any = support GET dan POST
Route::any('/film-search', [FilmController::class, 'search']);

Route::post('/film-save', [FilmController::class, 'save']);

Route::get('/film-delete/{film_id}', [FilmController::class, 'delete']);