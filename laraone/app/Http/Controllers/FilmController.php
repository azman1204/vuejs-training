<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Film;

class FilmController extends Controller {
    public function search(Request $req) {
        if ($req->isMethod('GET')) {
            return Film::paginate(20); // return data dlm format JSON
        } else {
            // post data / search
            $title = $req->title;
            return Film::where('title', 'like', "%$title%")->paginate(20);
        }
    }

    // save data
    public function save(Request $req) {
        $film = new Film();
        $film->title = $req->title;
        $film->description = $req->description;
        $film->rental_rate = $req->rental_rate;
        $film->language_id= $req->language;

        // data validation
        $data = $req->all();
        $rules = [
            'title' => 'required'
        ];
        $msg = ['title.required' => 'Title must no empty!'];
        $v = \Validator::make($data, $rules, $msg);

        $sts = new \stdClass();

        if ($v->fails()) {
            $sts->sts = 'fail';
            $sts->msg = 'Validation failed';
            $sts->err = $v->errors();
        } else {
            $film->save();
            $sts->sts = 'success';
            $sts->msg = 'Record has been saved';
        }
        
        return $sts;
    }

    public function delete($film_id) {
        $film = Film::find($film_id);
        $sts = $film->delete();
        $obj = new \stdClass();

        if ($sts) {
            $obj->sts = 'success';
            $obj->msg = "Film with id $film_id has been deleted...";
        } else {
            $obj->sts = 'fail';
            $obj->msg = "Film with id $film_id failed to delete...";
        }

        return $obj;
    }
}
