<?php
namespace App\Http\Controllers;
use App\Models\User;

class UserController extends Controller {

    // list all users
    public function listing() {
        $users = User::paginate(3);
        return $users;
    }
}