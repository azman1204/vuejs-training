@extends('master')
@section('content')

<div id="app">
    <h1>@{{ message }}</h1>
</div>

<script>
const app = Vue.createApp({
    data() {
        return {
            message: 'Hello World...test',
        }
    }
});

app.mount('#app');
</script>

@endsection