<?php

use Illuminate\Support\Facades\Route;

// SPA example
Route::get('/', function () {
    return view('index');
});

// MPA example
Route::get('/welcome', function () {
    return view('welcome');
});
