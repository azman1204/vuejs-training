import { createApp } from 'vue';
//import App from './App.vue';
import Home from './Home.vue';
import TheButton from './components/TheButton.vue'; // global registration
//import TheDirective from './components/TheDirective.vue';
import SearchEngine from './pages/SearchEngine.vue';
import PostRequest from './pages/PostRequest.vue';
//import TheMain from './pages/TheMain.vue';
import HelloVue from './components/HelloVue.vue';
import NotFound from './pages/NotFound.vue';
import { createWebHistory, createRouter } from 'vue-router';

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {path:'/', name: 'home', component: HelloVue},
        {path:'/post', component: PostRequest},
        {path:'/search', component: SearchEngine},
        {path:'/404', component: NotFound},
        {path: '/:catchAll(.*)', redirect: '/404'}
    ]
});

router.beforeEach((to, from) => {
    console.log(to, from);
    return true;
});

createApp(Home)
.component('TheButton', TheButton)
.use(router)
.mount('#app')
